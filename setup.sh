cd pyadolc
./bootstrap.sh
sudo python setup.py install



cd ../Ipopt-3.12.3
cd ThirdParty/Blas
./get.Blas
cd ../Lapack
./get.Lapack
cd ../ASL
./get.ASL
cd ../Mumps
./get.Mumps
cd ../Metis
./get.Metis

cd ../..
./configure  --enable-static coin_skip_warn_cxxflags=yes FFLAGS=-fallow-argument-mismatch
make
make test
make install

cd ../pyipopt
python setup.py build
sudo python setup.py install
